NAME := python
DEPENDS_ON := \
	asdf
CODE_EXTENSIONS := \
	ms-python.python \
	ms-toolsai.jupyter \
	tamasfe.even-better-toml
PYTHON_VERSION ?= latest

include $(CODY)
include $(SHARED)/code/code.mk

DEPENDS_ON += $(CODE_INSTALLER)

PIPX_BINARY = /usr/local/bin/pipx
POETRY_BINARY := /usr/local/bin/poetry
ifeq ($(CODENAME),bookworm)
PIPX_BINARY = /usr/bin/pipx
endif

ifeq ($(PKG_MANAGER),apt-get)
PYTHON_BUILD_DEPS := \
	/usr/include/bzlib.h \
	/usr/include/ncurses.h \
	/usr/include/ffi.h \
	/usr/include/readline/readline.h \
	/usr/include/openssl/ssl.h \
	/usr/include/sqlite3.h \
	/usr/include/tk.h \
	/usr/include/lzma.h
endif

.PHONY: install
ifeq ($(PKG_MANAGER),brew)
install: | \
	/usr/local/bin/python3 \
	/usr/local/bin/pip3 \
	$(PIPX_BINARY) \
	$(POETRY_BINARY) \
	$(HOME)/.asdf/shims/python \
	$(HOME)/.cargo/bin/uv \
	code-extensions
else
install: | sudo \
	/usr/bin/python3 \
	/usr/bin/python \
	/usr/bin/pip3 \
	$(PIPX_BINARY) \
	$(POETRY_BINARY) \
	$(HOME)/.asdf/shims/python \
	$(HOME)/.cargo/bin/uv \
	$(PYTHON_BUILD_DEPS) \
	code-extensions
endif

ifeq ($(PKG_MANAGER),apt-get)
/usr/bin/python3:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y \
		python3
/usr/bin/python:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y \
		python-is-python3
/usr/bin/pip3:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y \
		python3-pip
/usr/bin/poetry:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y \
		python3-poetry
/usr/bin/pipx:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y \
		pipx
/usr/local/bin/poetry:
	@$(SUDO) $(PIP) install poetry $(PIP_ARGS)
/usr/include/ncurses.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y libncurses5-dev libncursesw5-dev
/usr/include/ffi.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y libffi-dev
/usr/include/readline/readline.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y libreadline-dev
/usr/include/openssl/ssl.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y openssl
/usr/include/sqlite3.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y libsqlite3-dev
/usr/include/tk.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y tk-dev
/usr/include/lzma.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y liblzma-dev
/usr/include/bzlib.h:
	@$(call apt-update)
	@$(SUDO) $(APT) install -y libbz2-dev
/usr/local/bin/pipx:
ifeq ($(PKG_MANAGER),apt-get)
	@$(SUDO) $(APT) install -y \
		python3-venv
endif
	@$(SUDO) $(PIP) install pipx $(PIP_ARGS)
	@if ! printf '%s' "$$PATH" | grep -q "$$HOME/.local/bin" && ! grep -q "PATH.*/.local/bin" "$$HOME/.zshrc"; then \
		printf '%s\n' 'export PATH=$$PATH:$$HOME/.local/bin' >> "$$HOME/.zshrc"; \
	fi
else

ifeq ($(PKG_MANAGER),brew)
/usr/local/bin/pip3:
	@$(BREW) install pipenv
/usr/local/bin/pipx:
	@$(BREW) install pipx
	@echo 'export PATH=$$PATH:$$HOME/.local/bin' >> $(HOME)/.zshrc
/usr/local/bin/poetry:
	@$(BREW) install poetry
/usr/local/bin/python3:
	@$(BREW) install python
else
/usr/local/bin/poetry:
	@$(SUDO) $(PIP) install poetry $(PIP_ARGS)
/usr/local/bin/pipx:
	@$(SUDO) $(PIP) install pipx $(PIP_ARGS)
	@echo 'export PATH=$$PATH:$$HOME/.local/bin' >> $(HOME)/.zshrc
endif

/usr/bin/pip3: not-supported
/usr/bin/pipx: not-supported
/usr/bin/poetry: not-supported
/usr/bin/python3: not-supported
/usr/bin/python: not-supported
endif

$(HOME)/.asdf/shims/python: $(PYTHON_BUILD_DEPS)
	@asdf plugin add python
	@asdf install python $(PYTHON_VERSION)
	@asdf global python $(PYTHON_VERSION)
$(HOME)/.cargo/bin/uv:
	@curl -LsSf https://astral.sh/uv/install.sh | sh
	@if ! printf '%s' "$$PATH" | grep -q "$$HOME/.cargo/bin" && ! grep -q "PATH.*/.cargo/bin" "$$HOME/.zshrc"; then \
		printf '%s\n' 'export PATH=$$PATH:$$HOME/.cargo/bin' >> "$$HOME/.zshrc"; \
	fi

.PHONY: uninstall
uninstall:
